# Pico-8 Workspace

My [pico-8](https://www.lexaloffle.com/pico-8.php) things. 

Pico-8 is a fantasy console made by [Lexaloffle](https://www.lexaloffle.com).

## Cheatsheet

PNG included in repo.
Online [here](https://neko250.github.io/pico8-api/).

## Prerequisites

You need [pico-8](https://www.lexaloffle.com/pico-8.php) to run the **p8** files.
