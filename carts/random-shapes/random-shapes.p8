pico-8 cartridge // http://www.pico-8.com
version 16
__lua__

-- 
-- constants
--

rnd_max_reset = 1000
rnd_max_draw = 300

reset_config = {}
reset_config.after_frames_modifier = 10
reset_config.after_frames_base = 1000
reset_config.after_frames_next = 0
reset_config.frames_counter = 0

circ_config = {}
circ_config.radmin = 8
circ_config.radmax = 24
circ_config.posmin = 4
circ_config.posmax = 124
circ_config.chance = 10

rect_config = {}
rect_config.posmin = 4
rect_config.posmax = 124
rect_config.chance = 20

-- 
-- pico 8 loop
--

function _init()
  -- initial reset
  _screenreset()
  -- start music loops
  sfx(3)
  sfx(4)
end

function _update()
  reset_config.frames_counter = reset_config.frames_counter + 1
end

function _draw()
  -- reset if counter equals randomized 'after frames' value
  if (reset_config.frames_counter >= reset_config.after_frames_next) then
    _screenreset()
  end
  -- add random circle
  if(rnd(rnd_max_draw) <= circ_config.chance) then
    _draw_random_circle() 
  end
  -- add random rectancle
  if(rnd(rnd_max_draw) <= rect_config.chance) then
    _draw_random_rectangle()
  end
end

--
-- utility
--

function _screenreset()
  -- clear screen each draw
  cls()
  -- set background color
  rectfill(0,0,127,127,rnd(16))
  sfx(2)
  -- setup for next reset
  reset_config.after_frames_next = rnd(
    reset_config.after_frames_base
  ) + reset_config.after_frames_modifier
  reset_config.frames_counter = 0
end

function _draw_random_circle()
  local x = rnd(circ_config.posmax) + circ_config.posmin
  local y = rnd(circ_config.posmax) + circ_config.posmin
  local r = rnd(circ_config.radmax) + circ_config.radmin
  local color = rnd(16)
  circ(x, y, r, color)
  sfx(0)
end

function _draw_random_rectangle()
  local x0 = rnd(rect_config.posmax) + rect_config.posmin
  local y0 = rnd(rect_config.posmax) + rect_config.posmin
  local x1 = rnd(rect_config.posmax) + rect_config.posmin
  local y1 = rnd(rect_config.posmax) + rect_config.posmin
  local color = rnd(16)
  rect(x0, y0, x1, y1, color)
  sfx(1)
end
__sfx__
0001000006050090500b0500d0501205010050130501a05016050190401d0302003024020290102e010000000500001000010000000000000000000000000000000000000000000000000000000000000001e700
0004000012750167500f700007000a7500b750027000870000700287503470034700007000070000700007000d700007000070000700007000070000700007000070000700007000070000700007000070000700
00100000105500e650177502055007650076400562006620026200262002620016100161000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001a00181712015120121200e1200c1200912009120091200a1200a120091200912009120091200a120091201112011120141201712018120191200b1200b1200210000100001000010000100001000010000100
0018002008530085300853006530065300653003530035300d5300e5300e5300a5300e530105300c5301453013530135301353012530055300553005530055300d53010530135301553009530085301153011530
__music__
00 43424344

